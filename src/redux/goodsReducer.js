const defaultState = false;

export function goodsReducer(state = defaultState, action) {
    switch (action.type) {
        case 'GET_CARDS':
            return action.payload.cards;
        default:
            return state;
    }
}
