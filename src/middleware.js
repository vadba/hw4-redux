import { compose } from 'redux';
import thunk from 'redux-thunk';

const developmentEnvironment = process.env.NODE_ENV === 'development';
const devtools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = developmentEnvironment && devtools ? devtools : compose;

const middleware = [thunk];

export { composeEnhancers, middleware };
