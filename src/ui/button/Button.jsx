import s from './Button.module.scss';
import cn from 'classnames';

function Button(props) {
    const { onClick, className, children, text, backgroundColor } = props;

    return (
        <button onClick={onClick} className={cn(s[className], s.button, s[backgroundColor])}>
            {text || children}
        </button>
    );
}

export default Button;
